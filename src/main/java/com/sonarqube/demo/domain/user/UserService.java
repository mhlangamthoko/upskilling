package com.sonarqube.demo.domain.user;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> findAll();

    Optional<User> findById(Long id);

    User addUser(User user);
}
