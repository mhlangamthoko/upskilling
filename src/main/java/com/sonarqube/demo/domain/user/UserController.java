package com.sonarqube.demo.domain.user;

import com.sonarqube.demo.domain.dto.UserDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    private final ModelMapper modelMapper;

    @Autowired
    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @RequestMapping
    public List<UserDto> findAll() {
        return toDto(userService.findAll());
    }

    @RequestMapping("{id}")
    public UserDto findById(@PathVariable("id") Long id) {
        return toDto(userService.findById(id));
    }

    @PostMapping("add")
    public UserDto addUser(@RequestBody UserDto user) {
        User addedUser = userService.addUser(modelMapper.map(user, User.class));
        return modelMapper.map(addedUser, UserDto.class);
    }

    private UserDto toDto(Optional<User> optionalUser) {
        if (!optionalUser.isPresent()) {
            return null;
        }
        return modelMapper.map(optionalUser.get(), UserDto.class);
    }

    private List<UserDto> toDto(List<User> users) {
        return users
                .stream()
                .map(user -> modelMapper.map(user, UserDto.class))
                .collect(Collectors.toList());
    }

}
