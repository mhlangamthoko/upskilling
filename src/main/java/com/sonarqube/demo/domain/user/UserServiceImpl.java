package com.sonarqube.demo.domain.user;

import com.sonarqube.demo.domain.account.Account;
import com.sonarqube.demo.domain.account.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final AccountService accountService;

    @Autowired
    public UserServiceImpl(UserRepo userRepo, AccountService accountService) {
        this.userRepo = userRepo;
        this.accountService = accountService;
    }


    @Override
    public List<User> findAll() {
        return userRepo.findAll();
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepo.findById(id);
    }

    @Override
    @Transactional
    public User addUser(User user) {
        if (!user.getAccounts().isEmpty()) {
            List<Account> accounts = user.getAccounts();
            user.setAccounts(new ArrayList<>());
            userRepo.save(user);
            for (Account account : accounts) {
                accountService.createAccount(account, user);
            }
        } else {
            userRepo.save(user);
        }
        return user;
    }
}
