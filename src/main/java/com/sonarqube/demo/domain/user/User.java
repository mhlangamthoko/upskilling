package com.sonarqube.demo.domain.user;

import com.sonarqube.demo.domain.account.AbstractEntity;
import com.sonarqube.demo.domain.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "user_table")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends AbstractEntity {

    private String name;

    @OneToMany
    private List<Account> accounts;
}
