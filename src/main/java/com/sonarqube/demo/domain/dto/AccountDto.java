package com.sonarqube.demo.domain.dto;

import com.sonarqube.demo.domain.account.AccountType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private Long id;

    private AccountType type = AccountType.SAVINGS;

    private double balance;
}
