package com.sonarqube.demo.domain.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {

    @Query("select u.accounts from User u where u.id = :accHolderId")
    List<Account> findByAccountHolderId(@Param("accHolderId") Long accHolderId);
}
