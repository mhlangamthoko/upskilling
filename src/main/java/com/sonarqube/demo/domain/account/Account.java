package com.sonarqube.demo.domain.account;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Account extends AbstractEntity {

    private AccountType type = AccountType.SAVINGS;

    private double balance;
}
