package com.sonarqube.demo.domain.account;

public enum AccountType {
    SAVINGS,
    CHECK
}
