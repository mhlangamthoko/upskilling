package com.sonarqube.demo.domain.account;

import com.sonarqube.demo.domain.user.User;
import com.sonarqube.demo.domain.user.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepo accountRepo;
    private final UserRepo userRepo;

    @Autowired
    public AccountServiceImpl(AccountRepo accountRepo, UserRepo userRepo) {
        this.accountRepo = accountRepo;
        this.userRepo = userRepo;
    }

    @Override
    public List<Account> findByAccountHolderId(Long accHolderId) {
        return accountRepo.findByAccountHolderId(accHolderId);
    }

    @Override
    public List<Account> findAll() {
        return accountRepo.findAll();
    }

    @Override
    public Optional<Account> findById(Long id) {
        return accountRepo.findById(id);
    }

    @Override
    public Account save(Account account) {
        return accountRepo.save(account);
    }

    @Override
    @Transactional
    public Account createAccount(Account account, Long userId) {
        Optional<User> optionalUser = userRepo.findById(userId);
        if (!optionalUser.isPresent()) {
            throw new ApplicationException("User with id " + userId + " not found");
        }
        return createAccount(account, optionalUser.get());
    }

    @Override
    @Transactional
    public Account createAccount(Account account, User user) {
        List<Account> accounts = user.getAccounts();
        boolean accountAlreadyExistWithType = accounts
                .stream()
                .anyMatch(a -> a.getType().equals(account.getType()));
        if (accountAlreadyExistWithType) {
            throw new ApplicationException("User already has account of type: " + account.getType());
        }
        accountRepo.save(account);
        user.getAccounts().add(account);
        userRepo.save(user);
        return account;
    }

}
