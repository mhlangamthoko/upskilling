package com.sonarqube.demo.domain.account;

public class ApplicationException extends RuntimeException {
    public ApplicationException(String s) {
        super(s);
    }
}
