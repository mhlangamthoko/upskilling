package com.sonarqube.demo.domain.account;

import com.sonarqube.demo.domain.user.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public interface AccountService {
    List<Account> findByAccountHolderId(Long accHolderId);

    List<Account> findAll();

    Optional<Account> findById(Long id);

    Account save(Account account);

    Account createAccount(Account account, Long userId);

    @Transactional
    Account createAccount(Account account, User user);
}
