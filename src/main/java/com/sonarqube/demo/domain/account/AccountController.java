package com.sonarqube.demo.domain.account;

import com.sonarqube.demo.domain.dto.AccountDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("account")
public class AccountController {

    private final AccountService accountService;

    private final ModelMapper modelMapper;

    @Autowired
    public AccountController(AccountService accountService, ModelMapper modelMapper) {
        this.accountService = accountService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<AccountDto> findAll() {
        return toDto(accountService.findAll());
    }

    @GetMapping("{id}")
    public AccountDto findById(@PathVariable("id") Long id) {
        return toDto(accountService.findById(id));
    }

    @RequestMapping("holder/{id}")
    public List<AccountDto> findByAccountHolderId(@PathVariable("id") Long accHolderId) {
        return toDto(accountService.findByAccountHolderId(accHolderId));
    }

    @PostMapping("create/{userId}")
    public AccountDto create(@RequestBody AccountDto account, @PathVariable("userId") Long userId) {
        Account accountEntity = modelMapper.map(account, Account.class);
        return modelMapper.map(accountService.createAccount(accountEntity, userId), AccountDto.class);
    }

    private AccountDto toDto(Optional<Account> optionalAccount) {
        if (!optionalAccount.isPresent()) {
            return null;
        }
        return modelMapper.map(optionalAccount.get(), AccountDto.class);
    }

    private List<AccountDto> toDto(List<Account> accounts) {
        return accounts
                .stream()
                .map(account -> modelMapper.map(account, AccountDto.class))
                .collect(Collectors.toList());
    }

}
